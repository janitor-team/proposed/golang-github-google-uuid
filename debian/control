Source: golang-github-google-uuid
Section: devel
Priority: optional
Standards-Version: 4.4.0
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Dmitry Smirnov <onlyjob@debian.org>
Build-Depends: debhelper (>= 12~),
               dh-golang,
               golang-any
Homepage: https://github.com/google/uuid
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-google-uuid
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-google-uuid.git
XS-Go-Import-Path: github.com/google/uuid
Testsuite: autopkgtest-pkg-go

Package: golang-github-google-uuid-dev
Architecture: all
Depends: ${misc:Depends}
Description: generates and inspects UUIDs based on RFC 4122
 This package generates and inspects UUIDs based on RFC 4122
 (http://tools.ietf.org/html/rfc4122) and DCE 1.1: Authentication and
 Security Services.
 .
 This package is based on the "github.com/pborman/uuid" package. It differs
 from these earlier packages in that a UUID is a 16 byte array rather than
 a byte slice. One loss due to this change is the ability to represent an
 invalid UUID (vs a NIL UUID).
